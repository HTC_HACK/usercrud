package user;

import java.util.ArrayList;
import java.util.Scanner;

public class UserAction {

    User[] users;
    int index = 0, addId = 1;

    public UserAction(int userLength) {
        users = new User[userLength];
    }

    public boolean checkUser() {
        int count = 0;

        for (User user : this.users) {
            if (user == null) {
                count++;
            }
        }

        if (count > 0)
            return true;

        return false;
    }

    public void addUser() {

        Scanner scanner = new Scanner(System.in);
        User user = new User();

        System.out.print("Enter username : ");
        user.setUsername(scanner.nextLine());
        System.out.print("Enter phone : ");
        user.setPhone_number(scanner.nextLine());
        System.out.print("Enter email : ");
        user.setEmail(scanner.nextLine());
        System.out.print("Enter password : ");
        user.setPassword(scanner.nextLine());


        this.users[index++] = user;
        user.setId(addId);
        addId++;

        System.out.println("User successfully created");
    }

    public boolean userExist(int userId) {
        for (User user : users) {
            if (user.getId() == userId&&user!=null) {
                return true;
            }
        }
        return false;

    }

    public void updateUser() {

        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter user id : ");
        int userId = scanner.nextInt();

        if (userExist(userId) == false)
            System.out.println("User not found");

        for (User user : users) {
            if (user.getId() == userId) {
                Scanner userInfo = new Scanner(System.in);
                System.out.print("Enter username : ");
                user.setUsername(userInfo.nextLine());
                System.out.print("Enter phone : ");
                user.setPhone_number(userInfo.nextLine());
                System.out.print("Enter email : ");
                user.setEmail(userInfo.nextLine());
                System.out.print("Enter password : ");
                user.setPassword(userInfo.nextLine());
                System.out.println("This id : " + user.getId() + " updated successfully!");
                break;
            }
        }

    }

    public void getUserList() {
        int countUser = 0;
        for (User user : this.users) {
            if (user != null) {
                countUser++;
            }
        }

        if (countUser == 0) {
            System.out.println("No user yet");
        } else {
            for (User user : this.users) {
                if (user != null) {
                    System.out.println("|  " + user.getId() + " | " + user.getUsername() + " | " + user.getPhone_number() + " | " + user.getEmail() + " | " + user.getPassword());
                }
            }
        }
    }

    public void userDelete() {
        Scanner userId = new Scanner(System.in);
        System.out.print("Enter user id : ");

        Integer userid = userId.nextInt();

        if (!userExist(userid)) {
            System.out.println("User not found");
        } else {
            //[
            // 0 : {id:5,name:a},
            // 1 : {id:6,name:b},
            // 2 : {id:8,name:c}
            // ]
            for (User user : users) {
                if (user.getId() == userid) {
                    for (int i = 0; i < users.length; i++) {
                        if (users[i].getId() == userid) {
                            if (i < users.length&& i!= users.length-1) {

                                users[i] = null;
                                index--;

                                for (int j = i; (j != users.length - 1) && j < users.length; j++) {
                                    users[j] = users[j + 1];
                                    users[j + 1] = null;
                                }
                                System.out.println("Successfully deleted");
                                break;
                            } else if (i == users.length-1) {
                                users[i] = null;
                                index--;
                                System.out.println("Successfully deleted");
                                break;
                            }
                        }
                    }
                    break;
                }
            }

        }

    }
}
