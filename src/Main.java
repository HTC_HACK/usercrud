import user.User;
import user.UserAction;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        userCrud();

    }

    public static void userCrud()
    {
        Scanner scanner = new Scanner(System.in);

        System.out.print("How many users do you want : ");
        int userLength = new Scanner(System.in).nextInt();

        UserAction userAction = new UserAction(userLength);

        boolean avtive = true;

        while (avtive) {
            System.out.println("1.Add User\n2.Get User list\n3.Update User\n4.Delete User\n5.Exit");
            System.out.print("Enter option : ");
            int option = new Scanner(System.in).nextInt();
            switch (option) {
                case 1: {
                    if (userAction.checkUser()) {
                        userAction.addUser();
                        break;
                    } else {
                        System.out.println("User field is filled!");
                        break;
                    }
                }
                case 2: {
                    userAction.getUserList();
                    break;
                }
                case 3: {
                    userAction.updateUser();
                    break;

                }
                case 4: {
                    userAction.userDelete();
                    break;
                }
                case 5: {
                    avtive = false;
                    break;
                }
                default: {
                    System.out.println("Wrong option!");
                    break;
                }
            }

        }
    }
}
